## 2022-07-05

### Chair
- Ashlyn Knox

### Attendees
- Onuralp Sezer
- Francois Andrieu
- Akashdeep Dhar
- Graham White

### Check up
- How are you doing?
- How's the weather there?

### Agenda
- Picking chair for the next meeting
    - Who is it going to be? - Onuralp Sezer
- Engineering updates
    - Yes, this PR is still relevant - Timothee Ravier reached back
        - https://pagure.io/fedora-web/websites/pull-request/241#comment-173659
    - Opened and needs review
        - https://github.com/fedora-infra/mote/pull/335
    - Opened, reviewed, approved and merged
        - https://github.com/fedora-infra/mote/pull/323
        - https://github.com/fedora-infra/mote/pull/324
        - https://github.com/fedora-infra/mote/pull/325
        - https://github.com/fedora-infra/mote/pull/326
        - https://github.com/fedora-infra/mote/pull/327
        - https://github.com/fedora-infra/mote/pull/328
        - https://github.com/fedora-infra/mote/pull/329
        - https://github.com/fedora-infra/mote/pull/330
        - https://github.com/fedora-infra/mote/pull/331
        - https://github.com/fedora-infra/mote/pull/332
        - https://github.com/fedora-infra/mote/pull/333
        - https://github.com/fedora-infra/mote/pull/334
    - Meetbot Logs 2.0 is now deployed - Issues related to v1.0 are no longer relevant
        - Suggesting labelling all v1.0 issues with `legacy`
        - Shutting them down
    - A subgroup has been created to better organize themes and plugins
        - The subgroup can be found here https://gitlab.com/fedora/websites-apps/themes
        - Thanks Ryan Lerch for driving this forward
- Stakeholder updates
    - Ashlyn to get a meeting with Mairin
        - Workstation design is good to go for implementation
        - [Fedora FAV stack](https://gitlab.com/lilyx13/fedora-web-fav-stack)
- Objective updates
    - We are in the process of publishing about the recently deployed Meetbot Logs 2.0
        - https://discussion.fedoraproject.org/t/review-publish-meetbot-logs-2-0-is-out-now/40376
	- We now have basic information about the stakeholder's team in our official documentation
	    - https://pagure.io/fedora-websites/pull-request/1158
- Mindshare updates
	- Onuralp Sezer has been added as the Mindshare representative for the team
	    - https://pagure.io/fedora-websites/pull-request/1159
    - Updates from the Websites and Apps Team needs to be filled here as soon as possible
        - https://hackmd.io/dJ_71kmFRR-DX5YjQnLGpg?edit
    - Deciding on Mindshare representative's role on running the team
        - Setting up meeting agenda and running the meetings
        - Publish meeting logs regularly over at the meeting logs GitLab repo.
        - Regularly attending Mindshare meetings and representing the W&A team there
        - Regularly attending W&A meetings and sharing relevant Mindshare updates
- Miscellaneous
- Open floor
    - [Ashlyn's Drafting Sitemap](https://www.figma.com/file/JUpGEu4S5K9CzzG4rDThEW/Fedora-Sitemap?node-id=0%3A1)
