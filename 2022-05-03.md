## 2022-05-03

### Chair
- Vipul Siddharth (siddharthvipul)

### Attendees
- Graham White
- Michael Scherer 
- Gregory Lee Bartholomew

### Check up
- How are you doing?
- How's the weather there?
- Do you like cats?

### Agenda
- Picking chair for the next meeting
	- Who is going to be this time around? 
- Engineering updates
	- Issues addressed
		- https://pagure.io/fedora-web/websites/issue/242
		- https://pagure.io/fedora-web/websites/issue/243
	- Pull requests addressed
		- https://pagure.io/fedora-web/websites/pull-request/244
- Stakeholder updates
	- Please provide your views here
		- https://discussion.fedoraproject.org/t/fedora-workstation-front-page-revamp-first-cut-looking-for-feedback/37169
- Objective updates
	- Please go through the lacunaes in this documenation
		- https://docs.stg.fedoraproject.org/en-US/websites/
	- Reaching out to the websites revamp team for a representative for documentation (Was assigned to osezer)
	- We GOTTA plan for the F36 Release Party talk!!!!!
- Mindshare updates
	- We have our representative documentation up on staging
		- https://docs.stg.fedoraproject.org/en-US/websites/representatives/
	- We are still looking for representatives
		- Please spread the word 
		- If you're interested, please connect with t0xic0der
- Miscellaneous
	- Eid Mubarak, Onuralp sir! :D
		- Where's our share of Biriyani?
- Open floor

### Minutes

We got going a little late and decided with those of us on the meeting that it would be better to postpone until next week.  We agreed Vipul would chair once again and we would keep the same agenda items.
