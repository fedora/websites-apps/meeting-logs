## 2022-05-31

### Chair
- Onuralp Sezer

### Attendees
- Ashlyn Knox (lilyx)
- Akashdeep Dhar (t0xic0der)
- Francois Andrieu (darknao)
- Emma Kidney
- Nikolay
- Vipul Siddharth (siddharthvipul)
- Graham White (grahamwhiteuk)
- Gregory Bartholomew (glb)

### Check up
- How are you doing?
- How's the weather there?

### Agenda
- Picking chair for the next meeting
    - WHO IS IT GOING TO BE?
- Engineering updates
    - Unlimited SAML has been set up for the websites-apps GitLab subgroup
        - The repositories are now no longer restricted to those belonging to the `fedora` GitLab group or the `websites-apps` subgroup.
        - The repositories are now visible to everyone but changeable only by those who have the `developer` or `admin` role within the `websites-apps` subgroup.
        - Mapping between the [`websites-apps`](https://accounts.fedoraproject.org/group/websites-apps/?) and the `developer` role in the `websites-apps` GitLab subgroup has been established.
        - Mapping between the [`websites-apps-admin`](https://pagure.io/fedora-infrastructure/issue/10732) and the `admin` role in the `websites-apps` GitLab subgroup is in process.
        - Please reach out the FAS group sponsors to be added to these groups and henced, be given access to the GitLab subgroups - the new login location is here https://gitlab.com/groups/fedora/-/saml/sso
        - Many thanks to Ryan Lerch for putting time and efforts into making this thing happen - our efforts on the repos are now visible universally while ensuring curated contributions.
    - fedora-web/websites
        - Pull requests opened (or in progress) - Please feel free to review the following asynchronously
            - https://pagure.io/fedora-web/websites/pull-request/146 - reviewed
            - https://pagure.io/fedora-web/websites/pull-request/166 - reviewed
            - https://pagure.io/fedora-web/websites/pull-request/175 - reviewed
            - https://pagure.io/fedora-web/websites/pull-request/189 - reviewed
            - https://pagure.io/fedora-web/websites/pull-request/207 - reviewed
            - https://pagure.io/fedora-web/websites/pull-request/208 - reviewed
            - https://pagure.io/fedora-web/websites/pull-request/217 - reviewed
            - https://pagure.io/fedora-web/websites/pull-request/241 - reviewed
            - https://pagure.io/fedora-web/websites/pull-request/246 - reviewed
        - Pull requests merged
            - https://pagure.io/fedora-web/websites/pull-request/169
            - https://pagure.io/fedora-web/websites/pull-request/218
            - https://pagure.io/fedora-web/websites/pull-request/209
            - https://pagure.io/fedora-web/websites/pull-request/225
    - fedora-websites
        - Pull requests opened (or in progress) - Please feel free to review the following asynchronously
            - https://pagure.io/fedora-websites/pull-request/1151
            - https://pagure.io/fedora-websites/pull-request/1152
- Stakeholder updates
	- We have a new mockup from Emma Kidney and Mairin Duffy for Fedora IoT
	    - https://discussion.fedoraproject.org/t/fedora-iot-front-page-revamp-looking-for-feedback/39447
        - Please take some time to study the same and share feedback on the things you liked and the things that can improve
    - Ashlyn has a couple of demos that she wants to show about the static sites implementation for the Flock to Fedora websites
        - https://gitlab.com/lilyx13/fedora-vue-web-components
        - https://gitlab.com/lilyx13/simple-strapi-demo
        - https://vitejs.dev/
        - We should also take look to translation systems as well and check howe can we integrate into weblate (fedora translation system)
- Objective updates
	- Akashdeep has communicated the websites apprenticeship process formation task with Graham, who has gladly took up the responsibility.
	- We will keep you folks posted about the updates that we make in this regard and ask for feedbacks on how we can potentially improve.
- Mindshare updates
	- No updates
- Miscellaneous
- Open floor
