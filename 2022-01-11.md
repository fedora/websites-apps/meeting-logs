## 2022-01-11

### Chair
- Ashlyn Knox

### Attendees
- Graham White FAS grahamwhiteuk
- Akashdeep Dhar
- Michael Scherer FAS misc
- Subhangi Choudhary
- Ojong Enow
- Onuralp Sezer FAS thunderbirdtr
- Gregory Lee 

### Check up
- How are you doing?
- How's the weather there?
- Do you like cats?

### Agenda
- Intern progress check
    - Subhangi Choudhary - Mote
    - Ojong Enow - Fedora Websites
- GitLab project tracking
    - KanBan follow-up from previous meeting
- AOB

### Minutes

#### Intern Progress Check

##### Ojong Updates

Has been able to work on a few of the items assigned.

##### Subhangi Updates

Firstly, I worked on landing page and designed its UI. For this, I was told to design the calendar as suggested by Mairin and for this I used the calendar JS plugin called fullcalendar.io as it is very customizable and can be modified accordingly.The calendar fetches all the recent meetings happening in the current month and it can be seen as month and week view.I also made the today button which will become active in the week view once you navigate to the current date.On clicking the meeting, it redirects to meeting details page which shows all the information about the agenda discussed in the meeting.I was also told to widen the navbar control to occupy the full width taken by container for which I adjusted the aspect ratio but still as it varied and changed according to screen size, I did window reloading and set up the width so that the scrollbar gets removed and the calendar width is same in all screens. I also used bootstrap theming and font awesome to change the colors of the month, week, today, arrow buttons, and the meetings block. I set it according to the matching theme of the Fedora.Also when the calendar was being loaded, the meetings fetched used to display very late to which my mentor suggested to first render the empty calendar and then use the indeterminate spinner that is rendered by default when the modal is loaded up to be replaced or removed when the contents have arrived.For all this changes, I had to go through the docs of full calendar and make the changes. I also made the month and week view as custom buttons in header toolbar so that I can change the color and properties.

Then I implemented the search bar which shows first 5 meetings in the results list and in the dropdown when clicking on show more button, it opens the modal showing the list of all meetings with all the details like date, time and channel name.

#### GitLab Project Tracking

Allan Day was kind enough, after the meeting on 21st December, to share a couple of GitLab KanBan boards he manages for the Gnome project.  This allowed Graham to quickly assess the capabilities of GitLab for programme management.  GitLab calls them "issue boards".

GitLab's issue boards features:
* Multiple KanBan lanes with custom headers+colours and a count of the number of issues in each lane (GitLab calls these "lists").
* Filter by: Author, Assignee, Milestone, Release, Label
* Each list contains a set of issues that can have the following features (with some level of display on the KanBan board for easy viewing)
    * Assignees: the person/people working on the issue
    * Labels: a set of customisable labels to tag the issues with
    * Epic: whether the issue is part of a larger project/epic being tracked (long term e.g. 6-12 months)
    * Milestone: whether the issue contributes to a particular milestone (medium term e.g. 2-3 months)
    * Iteration: tracks the issue across sprints (short term e.g. 2 weeks)
    * Due Date: when the issue should be resolved
    * Weight: a number that can be used to assign some level of importance or complexity
    * Health Status: shows "on track", "needs attention" or "at risk"
* Starter Board at: https://gitlab.com/groups/fedora/websites-apps/-/boards
 

##### GitLab Notes
- Graham - check into migration from pagure to gitlab
  - name conversions for user names
    - potentially able to use a name mapping strategy
- It would be good to use FAS accounts to login to Gitlab
  - Current workflow:
    - create gitlab account
    - gitlab.com/fedora
      - saml authentication (sign in with your FAS account information)
      - this will bind your gitlab account to your fedora account, responding with message "SAML for fedora was added to your connected accounts"
- https://pagure.io/fedora-infrastructure/issue/10471
  
 
#### AOB

Onuralp talked through some design updates he's been thinking about.  Ojong to use Akashdeep as a backup contact should Onuralp be unavailable at any point.

#### Actions

Graham - check Pagure -> GitLab migration for user name conversion

Akashdeep - assign badges

**For Next Meeting**
- Check in with people who are going to the W&A program objective meeting.
  - potentially start using a gitlab kanban board for organization
